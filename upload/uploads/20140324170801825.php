
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<?php
	class FileUpload{
		private $filepath;
		private $allowtype=array('gif','jpg','png');
		private $maxsize;
		private $israndname=true;
		private $originName;
		private $tmpFileName;
		private $fileType;
		private $fileSize;
		private $newFilename;
		private $errorNum=0;
		private $errorMess="";
		
		function __construct($option=array()){
			foreach($option as $key=>$val){
				$key=strtolower($key);
				if(!in_array($key,get_class_vars(get_class($this)))){
					continue;
					}
					$this->setOption($key,$val);
				}
			}
		
		private function getError(){
			$str="上传文件<font color='red'>{$this->originName}</font>时出错：";
			switch($this->errorNum){
				case 4:$str.="没有文件被上传";break;
				case 3:$str.="文件只被部分上传";break;
				case 2:$str.="上传文件超过了HTML表单中MAX_FILE_SIZE指定的选项的值";break;
				case 1:$str.="上传文件超过了php.ini中upload_max_filesize选项的值";break;
				case -1:$str.="未允许的类型";break;
				case -2:$str.="文件过大，上传文件不能超过{$this->maxsize}个字节";break;
				case -3:$str.="上传失败";break;
				case -4:$str.="建立存放上传目录失败，请重指定上传目录";break;
				case -5:$str.="必须指定上传文件目录";break;
				default:$str.="未知错误";
				}
				return $str.'<br>';
			}
			
		private function checkFilePath(){
			if(empty($this->filepath)){
				$this->setOption('errorNum',-5);
				return false;
				}
				if(!file_exists($this->filepath)||!is_writable($this->filepath)){
					if(!@mkdir($this->filepath,0755)){
						$this->setOption('errorNum',-4);
						return false;
						}
					}
					return true;
			}	
		
		private function checkFileSize(){
			if($this->fileSize>$this->maxsize){
				$this->setOption('eoorrNum',-2);
				return false;
				}
				else{
						return true;
					}
			}	
			
		private function checkFileType(){
			if(in_array(strtolower($this->fileType),$this->allowtype)){
				return true;
				}
				else{
						$this->setOption('errorNum',-1);
						return false;
					}
			}	
		
		private function setNewFileName(){
			if($this->israndname){
				$this->setOption('newFilename',$this->proRandName());
				}
				else{
						$this->setOption('newFileName',$this->originName);
					}

			}
		
		private function proRandName(){
			$fileName=date("YmdHis").rand(100,999);
			return $fileName.'.'.$this->fileType;
			}
			
			
		private function setOption($key,$val){
			$this->$key=$val;
			}	
			
		function uploadFile($fileFiled){
			$return=true;
			if(!$this->checkFilePath()){
				$this->errorMess=$this->getError();
				return false;
				}
				
				$name=$_FILES[$fileFiled]['name'];
				$tmp_name=$_FILES[$fileFiled]['tmp_name'];
				$size=$_FILES[$fileFiled]['size'];
				$error=$_FILES[$fileFiled]['error'];
				
				if($this->setFiles($name,$tmp_name,$size,$error)){
					if($this->checkFileSize()&&$this->checkFileType()){
						$this->setNewFileName();
						if($this->copyFile()){
							return true;
							}
							else{
									$return=false;
								}
						}
						else{
								$return=false;
							}
					}
					else{
							$return=false;
						}
						if(!$return)
						$this->errorMess=$this->getError();
						return $return;	
		}
		
		private function copyFile(){
			if(!$this->errorNum){
				$filepath=rtrim($this->filepath,'/').'/';
				$filepath.=$this->newFilename;
				if(@move_uploaded_file($this->tmpFileName,$filepath)){
					return true;
					}
					else{
							$this->setOption('errorNum',-3);
							return false;
						}
				}
				else{
						return false;
					}
			}
			
			private function setFiles($name="",$tmp_name='',$size=0,$error=0){
				$this->setOption('errorNum',$error);
				if($error){
						return false;
					}
					$this->setOption('originNmae',$name);
					$this->setOption('tmpFileName',$tmp_name);
					$arrStr=explode('.',$name);
					$this->setOption('filetype',strtolower($arrStr[count($arrStr)-1]));
					$this->setOption('fileSize',$size);
					return true;
				}
				
				
	       function getNewFileName(){
			   return $this->newFilename;
			   }
			   
			function getErrorMess(){
				return $this->errorMess;
				}
	
	
	}
	
		